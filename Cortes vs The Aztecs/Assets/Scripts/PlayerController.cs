﻿/* 
 * Script: PlayerController
 * Author: Tom Brown
 * Description:
 * TODO: 
 *      - Make movement it's own monobehaviour, to use on Player and Enemy
 *      - Move ammo and weapon stuff out of here (into WeaponController)
*/

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	// Public Variables
	public float moveSpeed;
    public LayerMask floorLayer;
    public GameObject crosshair;
    public GameObject trailPrefab;


	// Private Variables
    private GameObject closestPickup;
    private Rigidbody rb;
    private WeaponController weaponController;
    private Vector3 crosshairPos;


    // Methods
    private void Start () 
    {
        rb = GetComponent<Rigidbody>();
        weaponController = GetComponent<WeaponController>();
        crosshairPos = Vector3.zero;

        // ====== DEBUG ======
        isRunning = true;
    }


    private void Update()
    {
        // ====== DEBUG COMMANDS ====== \\        
        // Reload the scene with P
        if (Input.GetKeyDown(KeyCode.P))
        {
            Scene loadedLevel = SceneManager.GetActiveScene ();
            SceneManager.LoadScene (loadedLevel.buildIndex);
        }

        // Spawn a prefab when SPACE is pressed
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject newTrail = Instantiate(trailPrefab, transform.position, transform.rotation);
        }
        // ====== END DEBUG CMDS ====== //

        // TODO - Scroll needs a delay between scroll iterations
        // Cycle weapons using the scroll wheel
        float mouseWheelAxis = Input.GetAxis("Mouse ScrollWheel");
        if (mouseWheelAxis > 0f)
        {
            weaponController.NextWeapon();
        }
        else if (mouseWheelAxis < 0f)
        {
            weaponController.PreviousWeapon();
        }

        // Set active weapon based on the input number
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            weaponController.ChangeWeapon(WeaponController.WeaponSlot.Small);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            weaponController.ChangeWeapon(WeaponController.WeaponSlot.Primary);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            weaponController.ChangeWeapon(WeaponController.WeaponSlot.Secondary);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            weaponController.ChangeWeapon(WeaponController.WeaponSlot.Thrown);
        }

        // Shoot if LMB pressed down
        if (Input.GetMouseButton(0))
        {
            weaponController.Attack();
        }

        // Reload if 'R' is pressed
        if (Input.GetKeyDown(KeyCode.R))
        {
            weaponController.ForceReload();
        }
    }


    private void FixedUpdate ()
    {
        UpdatePosition();
        UpdateRotation();
    }


    // TODO - Can this be moved into a 'movement' script on its own?
    //        Then this can be used for enemies too,
    //        and can be easily modified for status effects
    private void UpdatePosition ()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 movementVector = new Vector3(horizontal, 0 , vertical).normalized;

        rb.velocity = movementVector * moveSpeed;
    }


    private void UpdateRotation ()
    {
        // Draw a ray from the mouse position on screen, into the world
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(mouseRay.origin, mouseRay.direction * 1000, Color.blue);

        // Find the point of intersection with the floor (ignore everything else)
        RaycastHit hit;
        if (Physics.Raycast(mouseRay.origin, mouseRay.direction, out hit, Mathf.Infinity, floorLayer))
        {
            crosshairPos = hit.point;
        }

        // Calculate rotation
        Vector3 relativePosition = crosshairPos - transform.position;
        Quaternion newRotation = Quaternion.LookRotation(relativePosition);
        
        // Rotate the rigidbody
        rb.MoveRotation(newRotation);

        // Update the crosshair graphic
        UpdateCrosshair();
    }


    private void UpdateCrosshair ()
    {
        crosshair.transform.position = crosshairPos;
    }


    public Weapon GetWeapon ()
    {
        return weaponController.CurrentWeapon();
    }


    public void AddAmmo (int _amount)
    {
        //activeWeapon.AddAmmo(_amount);
    }


    public void RegisterPickup (GameObject _item)
    {
        // Do a check to see if it's closer than the current one

    }


    #region Debug Variables
    [Header("Debug Tools")]
    public bool showCrosshair = true;
    public bool showPlayerBounds = true;
    private bool isRunning = false;
    #endregion

    #region Debug Methods
    void OnDrawGizmos ()
    {
        if (isRunning)
        {
            if (showCrosshair)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(crosshairPos, 5f);
            }
        }
    }
    #endregion
}