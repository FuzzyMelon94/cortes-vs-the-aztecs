﻿/* 
 * Script: EnemyController
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
	// Public Variables
    [Range(1, 20)]
    public int health = 10;
    [Range(1, 10)]
    public int scoreValue = 5;
    [Range(10, 50)]
    public int goldValue = 5;
    [Range(1, 5)]
    public int bloodValue = 5;
    public float moveSpeed = 15;
    public float turnSpeed = 15;
    public GameObject target;
    public float targetDeadzone = 0.1f;
    public GameObject[] powerups;
	

	// Private Variables
    private GameManager gameManager;
    private bool isClimbing;
    private float currentHealth;
    private Rigidbody rb;
    private Vector3 climbingOffset;


    // Methods
    void Start () 
    {
        gameManager = GameManager.Instance;
        isClimbing = true;
        currentHealth = health;
        rb = GetComponent<Rigidbody>();

        // ====== DEBUG ======
        isRunning = true;
    }


    void FixedUpdate ()
    {
        if (target != null)
        {
            if (!isClimbing)
            {
                UpdatePosition();
                UpdateRotation();
            }
            else
            {
                rb.MovePosition(Vector3.MoveTowards(transform.position,
                                                    target.transform.position + climbingOffset,
                                                    moveSpeed * Time.deltaTime));

                //if (Vector3.Distance(transform.position, target.transform.position + climbingOffset) < targetDeadzone)
                if (transform.position == target.transform.position + climbingOffset)
                {
                    rb.MovePosition(new Vector3(transform.position.x, 0, transform.position.z));
                    rb.isKinematic = false;
                    rb.useGravity = true;
                    isClimbing = false;
                    FindTarget();
                }
            }
        }
    }


    private void UpdatePosition ()
    {
        rb.velocity = transform.forward * moveSpeed;
    }


    private void UpdateRotation ()
    {
        // Calculate rotation
        Vector3 relativePosition = target.transform.position - transform.position;
        Quaternion newRotation = Quaternion.LookRotation(relativePosition);
        
        // Rotate the rigidbody
        rb.MoveRotation(newRotation);
    }


    private void FindTarget ()
    {
        GameObject[] objectives = GameObject.FindGameObjectsWithTag("EnemyObjective");

        target = objectives[Random.Range(0, objectives.Length)];
    }


    private void DropLoot()
    {
        if (Random.Range(0, 25) == 0)
        {
            GameObject newPowerup = Instantiate(powerups[Random.Range(0, powerups.Length)],
                                                transform.position, transform.rotation);
        }
    }


    public void ClimbingTarget (GameObject _target, Vector3 _offset)
    {
        target = _target;
        climbingOffset = _offset;
    }


    public void TakeDamage (int _amount)
    {
        currentHealth -= _amount;

        if (currentHealth <= 0)
        {
            Die();
        }
    }


    public void Die ()
    {
        gameManager.IncreaseScore(scoreValue);
        gameManager.IncreaseGold(Mathf.RoundToInt(goldValue * Random.Range(0.75f, 2)));
        DropLoot();

        // Go back to the enemy object pool
        Destroy(gameObject);
    }


    public void AttackObjective (GameObject _target)
    {
        // Attack the player
        // Be a general annoyance
    }


    public void DestroyObjective (GameObject _target)
    {
        // Wail on the objective until it's broken
        // Find a new target
    }


    public void SacrificeObjective ()
    {
        // Do the animation to leap into the pit
        //      Keep running and swan dive
        //      Run up to it
        //          Look in, climb up, cannonball
        //          Climb up, swan dive
        //          Climb up, trust fall (forwards)
        
        gameManager.IncreaseBloodLevel(bloodValue);
        Destroy(transform.root.gameObject);
    }


    #region Debug Variables
    [Header("Debug Tools")]
    public bool showTarget = true;
    private bool isRunning = false;
    #endregion

    #region Debug Methods
    void OnDrawGizmos ()
    {
        if (isRunning)
        {
            if (showTarget && target != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(target.transform.position, 2f);
                Gizmos.DrawLine(transform.position, target.transform.position);
            }
        }
    }
    #endregion
}