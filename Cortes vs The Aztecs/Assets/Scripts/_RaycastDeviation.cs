﻿/* 
 * Script: _RaycastDeviation
 * Author: Tom Brown
 * Description: 
*/

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class _RaycastDeviation : MonoBehaviour
{
	// Public Variables
    [Header("Projectile Settings")]
    public float range = 10;        // Range of the projectile
    [Range(1, 100)]
    public int accuracy = 50;       // From 100% to x accuracy (0 to x% of maxSpread angle)
    [Range(0, 90)]
    public int maxSpread = 45;      // Maximum angle a projectile can leave the gun (specific to gun)
    [Range(1, 10)]
    public int projectileCount = 1; // Number of projectiles in shot
    
    [Header("Line Settings")]
    public bool showPerfectAccuracy = false;
    public bool showActualAccuracy = false;
    public bool showAccuracyBounds = false;
    public bool showSpreadBounds = false;
    [Range(0, 1.5f)]
    public float refreshInterval = 1;

    [Header("Line Colours")]
    public Color perfectRay;
    public Color actualRay;
    public Color accuracyBounds;
    public Color spreadBounds;


    [Header("Target")]
    public GameObject target;
    public float targetDistance;
    public Vector3 targetDirection;


	// Private Variables
    float halfSpread;
    float accuracyAngle;
    List<float> randomAngles;



    // Methods
    void Start ()
    {
        randomAngles = new List<float>();
        for (int i=0; i < projectileCount; i++)
        {
            randomAngles.Add(Random.Range(-accuracyAngle, accuracyAngle));
        }

        StartCoroutine("DrawRandomRays");
    }


    void Update ()
    {
        halfSpread = maxSpread * 0.5f;
        accuracyAngle = halfSpread * ((100 - accuracy) / 100.0f);

        // Generate new actual accuracy ray
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GenerateRandomAngles();
        }

        // Update target variables
        if (target)
        {
            targetDistance = Vector3.Distance(transform.position, target.transform.position);
            targetDirection = (target.transform.position - transform.position).normalized;
        }

        // Display the 100% accuracy ray
        if (showPerfectAccuracy)
        {
            Debug.DrawRay(transform.position,
                          targetDirection * ( range + 2.0f),
                          perfectRay);
        }
        
        // Display the actual accuracy ray
        if (showActualAccuracy)
        {
            foreach (float angle in randomAngles)
            {
                Debug.DrawRay(transform.position,
                              GetDirection(angle) * range,
                              actualRay);
            }   
        }

        // Display bounds of least accurate
        if (showAccuracyBounds)
        {
            DrawAccuracyBounds();
        }


        // Display bounds of least accurate
        if (showSpreadBounds)
        {
            DrawSpreadBounds();
        }
    }

    
    IEnumerator DrawRandomRays ()
    {
        while (true)
        {
            GenerateRandomAngles();
            yield return new WaitForSeconds(refreshInterval);
        }
    }


    void DrawAccuracyBounds ()
    {
        // Accuracy range min bound
        Debug.DrawRay(transform.position,
                      GetDirection(-accuracyAngle) * (range + 0.5f),
                      accuracyBounds);

        // Accuracy range max bound
        Debug.DrawRay(transform.position,
                      GetDirection(accuracyAngle) * (range + 0.5f),
                      accuracyBounds);
    }


    void DrawSpreadBounds ()
    {
        // Accuracy range min bound
        Debug.DrawRay(transform.position,
                      GetDirection(-halfSpread) * (range + 1f),
                      spreadBounds);

        // Accuracy range max bound
        Debug.DrawRay(transform.position,
                      GetDirection(halfSpread) * (range + 1f),
                      spreadBounds);
    }


    Vector3 GetDirection (float _angle)
    {
        return (Quaternion.Euler(0, _angle, 0) * targetDirection).normalized;
    }


    void GenerateRandomAngles ()
    {
        for (int i=0; i <= randomAngles.Count-1; i++)
        {
            randomAngles[i] = Random.Range(-accuracyAngle, accuracyAngle);
        }
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}