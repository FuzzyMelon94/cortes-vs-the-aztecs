﻿/* 
 * Script: Powerup
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class Powerup : MonoBehaviour
{
	public enum PowerupType
    {
        Health,
        Ammo
    }
    
    // Public Variables
	public PowerupType powerupType = PowerupType.Health;
    public int amount;

	// Private Variables
    

    // Methods
    


    private void OnTriggerEnter (Collider _collider) 
    {
        if (_collider.transform.root.tag == "Player")
        {
            PlayerController player = _collider.transform.root.GetComponent<PlayerController>();

            switch (powerupType)
            {
                case PowerupType.Health:
                break;

                case PowerupType.Ammo:
                player.AddAmmo(amount);
                break;
            }

            Destroy(transform.root.gameObject);
        }
    }




    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}