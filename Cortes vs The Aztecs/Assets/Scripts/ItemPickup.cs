﻿/* 
 * Script: ItemPickup
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
	// Public Variables
	public bool requiresKeypress = false;   // Is a keypress required to pick this up

	// Private Variables
    private bool canPickUp;                 // Is someone within the pickup radius
    private GameObject player;              // The player trying to pick this up


    // Methods
    void Start ()
    {
        canPickUp = false;
    }

    void OnTriggerEnter (Collider _collider)
    {
        canPickUp = true;
        player = _collider.transform.root.gameObject;
    }


    void OnTriggerExit (Collider _collider)
    {
        canPickUp = false;
        player = null;
    }




    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}