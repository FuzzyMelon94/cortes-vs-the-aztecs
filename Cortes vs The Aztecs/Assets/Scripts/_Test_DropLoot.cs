﻿/* 
 * Script: _Test_DropLoot
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class _Test_DropLoot : MonoBehaviour
{
	// Public Variables
	

	// Private Variables
    private ItemSpawner itemSpawner;
    

    // Methods
    private void Start ()
    {
        itemSpawner = ItemSpawner.Instance;
    }
    
    
    private void OnMouseDown()
    {
        Vector2 randPos = Random.insideUnitCircle * 5;
        Vector3 newPos = new Vector3(transform.position.x + randPos.x,
                                     transform.position.y,
                                     transform.position.z + randPos.y);

        print(name + " generating loot at " + newPos);
        itemSpawner.GenerateLoot(newPos);
    }




    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}