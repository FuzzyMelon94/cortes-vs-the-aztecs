﻿/* 
 * Script: Projectile
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    // NOTES
    /*  
     *  Convert this into a Raycast from the gun muzzle
     *  Set the start point at the muzzle position
     *  Set the destination point at the hit.point position (or max range)
     *  Display a nice particle effect along that path
     *  Should allow for multiple targets to be struck (piercing rounds)
     */

	// Public Variables
    public float weight = 5;

	// Private Variables
    private bool isLive;
    private Rigidbody rb;
    private float gravity;
    private float speed;
    private int damage;
    private float distance;
    private float range;
    private AnimationCurve damageFalloff;
    private Vector3 startPos;
    

    // Methods
    private void Start ()
    {
        rb = GetComponent<Rigidbody>();
        gravity = 0;
        distance = 0;
        startPos = transform.position;

        Enable(true);
    }


    private void FixedUpdate ()
    {
        if (isLive)
        {
            rb.velocity = (transform.forward * speed) + (Vector3.down * gravity);
            distance = Vector3.Distance(startPos, transform.position);

            if (distance > range)
            {
                AtMaxRange();
            }
        }
    }


    private void OnCollisionEnter (Collision _collision)
    {
        if (_collision.collider.transform.root.tag == "Enemy")
        {
            // CLEAN THIS UP - OBJECT POOLS
            DealDamage(_collision.collider.transform.root.gameObject);
            Destroy(transform.root.gameObject);
        }

        if (_collision.collider.tag == "Obstacle")
        {
            speed = 0;
            gravity = weight;
        }

        if (_collision.collider.transform.root.tag == "Floor")
        {
            Enable(false);
        }
        
    }


    protected void DealDamage (GameObject _object)
    {
        // Check how far this projectile travelled before impact
        // Calculate how much damage it should do using the distance past range
        // Reference the AnimationCurve to get a nice falloff curve

        EnemyController enemy = _object.GetComponent<EnemyController>();
        enemy.TakeDamage(damage);
    }


    public virtual void Enable (bool _state)
    {
        // Turn off the rigidbody <== Not sure why I need the if here - to be corrected
        if (rb)
            rb.isKinematic = !_state;
        
        // Disable the colliders
        foreach (Collider c in GetComponentsInChildren<Collider>())
        {
            c.enabled = _state;
        }

        // Stop movement
        isLive = _state;
    }


    public virtual void AtMaxRange ()
    {
        gravity = Mathf.Lerp(gravity, weight, Time.deltaTime * 2f);
    }


    public void Setup (float _speed, int _damage, float _range)
    {
        speed = _speed;
        damage = _damage;
        range = _range;;
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}