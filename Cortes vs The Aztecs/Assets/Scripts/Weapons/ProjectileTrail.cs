﻿/* 
 * Script: ProjectileTrail
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class ProjectileTrail : MonoBehaviour
{
	// Public Variables
	public ParticleSystem trailParticles;
    public ParticleSystem impactParticles;

	// Private Variables
    private bool isImpact;
    private bool isMoving;
    private float speed;
    private Vector3 endPos;
    

    // Methods
    void Update ()
    {
        if (isMoving)
        {
            // Move projectile to target position
            transform.position = Vector3.MoveTowards(transform.position,
                                                    endPos,
                                                    speed * Time.deltaTime);

            if (transform.position == endPos)
            {
                isMoving = false;

                if (isImpact && impactParticles != null)
                {
                    PlayImpact();
                }
            }
        }
    }


    float CalculateLifetime ()
    {
        float impactDelay = impactParticles == null ? 0 : impactParticles.main.duration; 

        return trailParticles.main.duration + impactDelay;
    }


    void PlayImpact ()
    {
        impactParticles.Play();
    }


    IEnumerator ResetToPool (float _delay)
    {
        yield return new WaitForSeconds(_delay);
        
        // TODO - Return the effect to the object pool
        Destroy(gameObject);
    }


    public void SetupParticles (Vector3 _startPos, Vector3 _endPos, float _speed, bool _isImpact=false)
    {
        // Set position
        transform.position = _startPos;

        // Set rotation
        Vector3 relative = _endPos - transform.position;
        transform.rotation = Quaternion.LookRotation(relative, Vector3.up);

        // Set other variables
        endPos = _endPos;
        speed = _speed;
        isImpact = _isImpact;

        // Start timer
        StartCoroutine("ResetToPool", CalculateLifetime());
        trailParticles.Play();
        isMoving = true;
    }



    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}