﻿/* 
 * Script: WeaponMelee
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class WeaponMelee : Weapon
{
	// Public Variables
	

	// Private Variables
    private MeleeWeaponObject melee;
    

    // Methods
    private void Start ()
    {
        melee = (MeleeWeaponObject)base.weapon;
    }


    public override void Attack (float _desiredRange)
    {
        // Swing yo stik
    }



    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}