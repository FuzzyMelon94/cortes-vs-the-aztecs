﻿/* 
 * Script: ExplosiveProjectile
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class ExplosiveProjectile : Projectile
{
	// Public Variables
    public float explosionRadius;
    public float explosionForce;
    public float damageMultiplier;
    public LayerMask enemyLayer;
	public ParticleSystem smokeParticle;

	// Private Variables

    

    // Methods
    private void OnCollisionEnter (Collision _collision)
    {
        if (_collision.collider.transform.root.tag != "Player")
        {
            Detonate(_collision.transform.position);
        }
    }


    private void Detonate (Vector3 _position)
    {
        Enable(false);

        Collider[] hits = Physics.OverlapSphere(_position,
                                                explosionRadius,
                                                enemyLayer);

        // Mess up all enemies that were hit
        foreach (Collider hit in hits)
        {
            // If working on this later, switch from protected
            // to override, damage should vary with distance
            // from the explosion origin
            base.DealDamage(hit.transform.root.gameObject);
        }

        // Do particles
        smokeParticle.Play();
    }


    public override void Enable (bool _state)
    {
        // Do everything a normal projectile would do
        base.Enable(_state);

        // Now do more!
        transform.Find("Model").gameObject.SetActive(_state);
    }


    public override void AtMaxRange ()
    {
        Detonate(transform.position);
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}