﻿/* 
 * Script: WeaponThrown
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class WeaponThrown : Weapon
{
	// Public Variables
	

	// Private Variables
    

    // Methods
    public override void Attack (float _desiredRange=0)
    {
        print(weapon.name + " bein' thrown!");
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}