﻿/* 
 * Script: ProjectileShell
 * Author: Tom Brown
 * Description: 
*/

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class ProjectileShell : MonoBehaviour
{
	// Public Variables


	// Private Variables
    private float sinkDuration;     // Time it takes for the shell to sink below ground
    private Rigidbody myRigidbody;

    // Methods
    void Awake ()
    {
        sinkDuration = 10.0f;
        myRigidbody = GetComponent<Rigidbody>();
    }


    IEnumerator CleanUp (float _delay)
    {
        yield return new WaitForSeconds(_delay);

        RigidbodyActive(false);

        float percent = 0;
        float sinkSpeed = 1 / sinkDuration;

        Vector3 startPos = transform.position;
        Vector3 endPos = startPos + (Vector3.down * 2);

        while (percent < 1)
        {
            percent += Time.deltaTime * sinkSpeed;
            transform.position = Vector3.MoveTowards(startPos,
                                                     endPos,
                                                     percent);
            yield return null;
        }

        // TODO - Update this so that it returns to an object pool
        Destroy(gameObject);
    }


    public void RigidbodyActive (bool _state)
    {
        myRigidbody.isKinematic = !_state;       // Is it controlled by code
        myRigidbody.detectCollisions = _state;   // Does it detect collisions
    }
    

    public void SetupShell (float _force, float _randomness, float _timeToLive)
    {
        float randomOffset = Random.Range(1, _randomness);
        RigidbodyActive(true);
        myRigidbody.AddForce(transform.forward * (_force * randomOffset));
        myRigidbody.AddTorque(Random.insideUnitSphere * _force);

        StartCoroutine("CleanUp", _timeToLive);
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}