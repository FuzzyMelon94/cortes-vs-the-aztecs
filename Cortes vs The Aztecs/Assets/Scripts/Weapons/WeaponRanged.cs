﻿/* 
 * Script: WeaponRanged
 * Author: Tom Brown
 * Description: 
*/

using System.Collections.Generic;
using System.Collections;
using UnityEngine;


public class WeaponRanged : Weapon
{
	// Public Variables
    [Header("Ranged Weapon Variables")]
    public int currentClip;                 // Ammo left in the current clip
    public int remainingAmmo;               // Ammo left in player's inventory
    public Transform[] muzzles;             // Projectile spawn point(s)

    [Header("Projectile Settings")]
    public Transform shellEjector;          // Where to spawn shell ejection effect
    public float ejectionForce = 90;        // How fast shells are ejected
    [Range(1.0f, 3.0f)]
    public float ejectionRandomness = 2;    // How random the force is for each shell    
	

	// Private Variables
    private AmmoObject currentAmmo;         // Ammo type currently in use
    private bool isReloading;               // Is the gun being reloaded
    private float accuracyAngle;            // Range of deviation projectile may leave muzzle at
    private float halfMuzzleSpread;         // Half of the accuracy angle (for calculations)
    private float lastFired;                // Time projectile was last fired
    private int currentMuzzle;              // Which muzzle to use
    private GameObject[] projectilePool;    // An object pool for the projectiles
    private LayerMask weaponHitLayers;      // What should the weapon be able to shoot
    private RangedWeaponObject ranged;      // Reference to the ranged object
    

    // Methods
    private void Start ()
    {
        ranged = (RangedWeaponObject)base.weapon;
        ImportWeaponStats();
        isReloading = false;
        lastFired = Time.time;
        currentMuzzle = 0;
        weaponHitLayers = WeaponManager.Instance.weaponHitLayers;

        // ====== DEBUG ======
        isRunning = true;
    }


    /// <summary>Imports the weapon stats, should be called when ammo/mods are changed.</summary>
    private void ImportWeaponStats ()
    {
        currentAmmo = ranged.defaultAmmo;
        halfMuzzleSpread = ranged.muzzleSpread * 0.5f;
        accuracyAngle = halfMuzzleSpread * ((100 - ranged.accuracy) * 0.01f);
    }


    /// <summary>Creates a trail effect for the fired projectile.</summary>
    /// <param name="_muzzle">Where the projectile is spawned.</param>
    /// <param name="_target">The destination of the projectile.</param>
    /// <param name="_isImpact">Does this projectile impact something?</param>
    private void PlayEffectTrails (Transform _muzzle, Vector3 _target, bool _isImpact)
    {
        // TODO - Update these effects to use object pools to help performance

        // Generate bullet trail effect
        GameObject projectile = Instantiate(currentAmmo.projectilePrefab,
                                            _muzzle.position,
                                            _muzzle.rotation);
        projectile.GetComponent<ProjectileTrail>().SetupParticles(_muzzle.position,
                                                                  _target,
                                                                  currentAmmo.velocity,
                                                                  _isImpact);
    }


    /// <summary>Play an effect for muzzle flash when a projectile is fired.</summary>
    private void PlayEffectMuzzle ()
    {
        // Generate muzzle flash effect
    }


    /// <summary>Play a shell ejection effect when a projectile is fired.</summary>
    private void PlayEffectShell ()
    {
        // Generate shell ejection effect
        GameObject shell = Instantiate(currentAmmo.shellCasingPrefab,
                                       shellEjector.position,
                                       shellEjector.rotation);
        shell.GetComponent<ProjectileShell>().SetupShell(ejectionForce,
                                                         ejectionRandomness,
                                                         currentAmmo.timeToLive);
    }


    /// <summary>Get a direction based on the weapon's forward.</summary>
    /// <param name="_angle">The angle to calculate.</param>
    /// <returns>A Vector3 with the direction of an angle from the weapon's forward.</returns>
    private Vector3 GetDirection (float _angle)
    {
        return (Quaternion.Euler(0, _angle, 0) * transform.forward).normalized;
    }


    /// <summary>Reload the weapon, provide a delay to simulate reload time.</summary>
    private IEnumerator ReloadSequence ()
    {
        isReloading = true;
        yield return new WaitForSeconds(ranged.reloadTime);

        int neededAmmo = ranged.clipSize - currentClip;

        if (remainingAmmo >= neededAmmo)
        {
            currentClip += neededAmmo;
            remainingAmmo -= neededAmmo;
        }
        else
        {
            currentClip += remainingAmmo;
            remainingAmmo = 0;
        }

        isReloading = false;
    }


    /// <summary>Fire the weapon at the desired range, if set.</summary>
    /// <param name="_desiredRange">Desired range for explosive weapons,
    /// max range if nothing is entered.</param>
    public override void Attack (float _desiredRange=0)
    {
        if (muzzles.Length < 1)
        {
            Debug.Log(weapon.name + " doesn't appear to have any muzzle points...");
            return;
        }

        if (_desiredRange == 0)
        {
            _desiredRange = ranged.range;
        }

        if (lastFired + ranged.fireRate < Time.time &&
            currentClip > 0)
        {
            bool isImpact = false;
            float angle =  0;
            RaycastHit hit;
            Vector3 startPos = muzzles[currentMuzzle].position;
            Vector3 endPoint = Vector3.zero;
            List<float> angles = new List<float>();
            
            for (int i=0; i<currentAmmo.projectileCount; i++)
            {
                // Generate new angle for each projectile
                angle = Random.Range(-accuracyAngle, accuracyAngle);
                angles.Add(angle);  // TODO - Remove, this is for debugging only

                if (Physics.Raycast(muzzles[currentMuzzle].position,
                                    GetDirection(angle),
                                    out hit,
                                    _desiredRange,
                                    weaponHitLayers))
                {
                    endPoint = hit.point;
                    isImpact = true;
                    EnemyController enemy = hit.collider.transform.parent.parent.GetComponent<EnemyController>();
                    if (enemy != null)
                    {
                        enemy.TakeDamage(ranged.damage);
                    }
                }
                else
                {
                    endPoint = muzzles[currentMuzzle].position + (GetDirection(angle) * _desiredRange);
                }

                PlayEffectTrails(muzzles[currentMuzzle], endPoint, isImpact);
            }

            PlayEffectShell();

            // Move to next muzzle
            if (muzzles.Length > 1)
            {
                currentMuzzle ++;

                // Loops through the muzzles
                if (currentMuzzle >= muzzles.Length)
                {
                    currentMuzzle = 0;
                }
            }

            // Update variables
            lastFired = Time.time;
            currentClip --;

            // ==== DEBUG ==== \\
            ShowAccuracyDebugRays(angles);
        }

        // When clip is empty
        if (currentClip <= 0)
        {
            Reload();
        }
    }


    /// <summary>Reload the weapon.</summary>
    public void Reload ()
    {
        // Fill from reserves
        if (remainingAmmo > 0 && !isReloading)
        {
            StartCoroutine(ReloadSequence());
        }

        // Or tell the user to find more ammo
        if (remainingAmmo <= 0)
        {
            Debug.Log("No more ammo in " + ranged.name);
        }
    }


    /// <summary>Get the current ammo levels for this weapon.</summary>
    /// <returns>A 2 element int array containing the remaining clip count, and total remaining ammo.</returns>
    public override int[] GetAmmo ()
    {
        return new int[2] {currentClip, remainingAmmo};
    }


    #region Debug Variables
    [Header("Debug Tools")]
    public bool showPerfectRay = false;
    public bool showActualRays = false;
    public bool showAccuracyBounds = false;
    public bool showSpreadBounds = false;
    public float rayDisplayDuration = 1.0f;

    private Color perfectRayColour = Color.green;
    private Color actualRayColour = Color.yellow;
    private Color spreadBoundsColour = Color.blue;
    private Color accuracyBoundsColour = Color.cyan;
    private List<float> actualRays = new List<float>();


    private bool isRunning = false;
    #endregion

    #region Debug Methods
    void OnDrawGizmos ()
    {
        if (isRunning)
        {
            if (showSpreadBounds)
            {
                // Accuracy range min bound
                Gizmos.color = spreadBoundsColour;
                Gizmos.DrawRay(muzzles[currentMuzzle].position,
                               GetDirection(-halfMuzzleSpread) * (ranged.range + 1f));

                // Accuracy range max bound
                Gizmos.color = spreadBoundsColour;
                Gizmos.DrawRay(muzzles[currentMuzzle].position,
                               GetDirection(halfMuzzleSpread) * (ranged.range + 1f));
            }

            if (showAccuracyBounds)
            {
                // Accuracy range min bound
                Gizmos.color = accuracyBoundsColour;
                Gizmos.DrawRay(muzzles[currentMuzzle].position,
                               GetDirection(-accuracyAngle) * (ranged.range + 0.5f));

                // Accuracy range max bound
                Gizmos.color = accuracyBoundsColour;
                Gizmos.DrawRay(muzzles[currentMuzzle].position,
                               GetDirection(accuracyAngle) * (ranged.range + 0.5f));
            }

            // Display the 100% accuracy ray
            if (showPerfectRay)
            {
                Gizmos.color = perfectRayColour;
                Gizmos.DrawRay(muzzles[currentMuzzle].position,
                               transform.forward * ranged.range);
            }
            
            // Display the actual accuracy ray
            if (showActualRays)
            {
                Gizmos.color = actualRayColour;
                foreach (float angle in actualRays)
                {
                    Gizmos.DrawRay(muzzles[currentMuzzle].position,
                                   GetDirection(angle) * ranged.range);
                }   
            }
        }
    }


    private void ShowAccuracyDebugRays (List<float> _angles)
    {
        actualRays.Clear();
        actualRays = _angles;
    }
    #endregion
}