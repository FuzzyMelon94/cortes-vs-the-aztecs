﻿/* 
 * Script: WeaponController
 * Author: Tom Brown
 * Description: 
*/

using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public enum WeaponSlot
    {
        Small,
        Primary,
        Secondary,
        Thrown
    }

	// Public Variables
	public WeaponSlot activeWeapon = WeaponSlot.Small;
    public Transform handTransform;         // Weapon position when active

    [Header("Equipped Weapons")]
    public Weapon weaponSmall;              // Weapon in small holster - one handed
    public Weapon weaponPrimary;            // Weapon in primary holster - two handed
    public Weapon weaponSecondary;          // Weapon in secondary holster - two handed
    public Weapon weaponThrown;             // Weapon in thrown holster

    [Header("Holster Positions")]
    public Transform holsterSmall;          // Position of the small weapon holster
    public Transform holsterPrimary;        // Position of the primary weapon holster
    public Transform holsterSecondary;      // Position of the secondary weapon holster
    public Transform holsterThrown;         // Position of the thrown holster

    [Header("Equipped Ammo")]
    public Ammo ammoTypePistol;
    public Ammo ammoTypeShotgun;
    public Ammo ammoTypeAssault;
    public Ammo ammoTypeSniper;
    public Ammo ammoTypeThrown;
    

	// Private Variables
    private int currentAmmoPistol;
    private int specialAmmoPistol;
    private int currentAmmoShotgun;
    private int specialAmmoShotgun;
    private int currentAmmoAssault;
    private int specialAmmoAssault;
    private int currentAmmoSniper;
    private int specialAmmoSniper;
    private int currentAmmoThrown;
    private int specialAmmoThrown;

    private UIManager uiManager;
    

    // Methods
    void Start ()
    {
        uiManager = UIManager.Instance;
    }


    /// <summary>Gets the weapon in the specified slot.</summary>
    /// <param name="_slot">The weapon slot.</param>
    /// <returns>A weapon object.</returns>
    private Weapon GetWeapon (WeaponSlot _slot)
    {
        switch (_slot)
        {
            case WeaponSlot.Small:
                return weaponSmall;

            case WeaponSlot.Primary:
                return weaponPrimary;

            case WeaponSlot.Secondary:
                return weaponSecondary;

            case WeaponSlot.Thrown:
                return weaponThrown;

            default:
                return null;
        }
    }


    private void Holster (WeaponSlot _slot, bool _state=true)
    {
        Weapon weapon = GetWeapon(_slot);

        // Stop early if the weapon is null...
        if (weapon == null)
        {
            return;
        }

        // Make the gun a parent of the correct holster
        switch (_slot)
        {
            case WeaponSlot.Small:
                weapon.transform.parent = _state ? holsterSmall : handTransform;
                break;
            
            case WeaponSlot.Primary:
                weapon.transform.parent = _state ? holsterPrimary : handTransform;;
                break;
            
            case WeaponSlot.Secondary:
                weapon.transform.parent = _state ? holsterSecondary : handTransform;;
                break;
            
            case WeaponSlot.Thrown:
                weapon.transform.parent = _state ? holsterThrown : handTransform;;
                break;
        }
        
        // Reset the rotations to match the parent
        weapon.transform.localPosition = Vector3.zero;
        weapon.transform.localRotation = Quaternion.identity;
    }


    /// <summary>Attack with the weapon.</summary>
    public void Attack ()
    {
        Weapon weapon = GetWeapon(activeWeapon);
        
        if (weapon != null)
        {
            weapon.Attack();
        }

        // Update UI - current clip
        uiManager.UpdateWeapon(CurrentWeapon());
    }


    /// <summary>Force the weapon to reload now.</summary>
    public void ForceReload ()
    {
        WeaponRanged weapon = GetWeapon(activeWeapon) as WeaponRanged;

        if (weapon != null && weapon.weapon.type == Weapon.Type.Ranged)
        {
            weapon.Reload();
        }

        // Update UI - current clip & ammo
        uiManager.UpdateWeapon(CurrentWeapon());
    }


    /// <summary>Wrapper to holster the current weapon, switch to the next one, and draw it.</summary>
    /// <param name="_slot">The weapon slot to change to.</param>
    public void ChangeWeapon (WeaponSlot _slot)
    {
        // Holster current weapon
        Holster(activeWeapon);

        // Change to weapon in _slot
        activeWeapon = _slot;

        // Unholster the new weapon
        Holster(activeWeapon, false);

        // Update UI
        uiManager.UpdateWeapon(CurrentWeapon());
    }


    /// <summary>Change to the next weapon slot.</summary>
    public void NextWeapon ()
    {
        switch (activeWeapon)
        {
            case WeaponSlot.Small:
                ChangeWeapon(WeaponSlot.Primary);
                break;

            case WeaponSlot.Primary:
                ChangeWeapon(WeaponSlot.Secondary);
                break;

            case WeaponSlot.Secondary:
                ChangeWeapon(WeaponSlot.Thrown);
                break;

            case WeaponSlot.Thrown:
                ChangeWeapon(WeaponSlot.Small);
                break;
        }
    }


    /// <summary>Change to the previous weapon slot.</summary>
    public void PreviousWeapon ()
    {
        switch (activeWeapon)
        {
            case WeaponSlot.Small:
                ChangeWeapon(WeaponSlot.Thrown);
                break;

            case WeaponSlot.Primary:
                ChangeWeapon(WeaponSlot.Small);
                break;

            case WeaponSlot.Secondary:
                ChangeWeapon(WeaponSlot.Primary);
                break;

            case WeaponSlot.Thrown:
                ChangeWeapon(WeaponSlot.Secondary);
                break;
        }
    }



    public void AddAmmo (Ammo.Type _type, int _amount)
    {
        // If a value hasn't been entered, use max ammo
        // remainingAmmo += (_ammo == 0 ? ranged.maxAmmo : _ammo);
        // remainingAmmo = Mathf.Clamp(remainingAmmo, 0, ranged.maxAmmo);

        // If clip is empty and not already, reload
        // if (currentClip <=0 && !isReloading)
        // {
        //     StartCoroutine(Reload());
        // }

        // Update the UI
        // UpdateUI();
    }


    /// <summary>Pickup the weapon.</summary>
    /// <param name="_weapon">The weapon to be picked up.</param>
    public void TakeWeapon (Weapon _weapon)
    {
        // TODO - Can this be done in a nicer way?

        // If it's a thrown weapon
        if (_weapon.weapon.type == Weapon.Type.Thrown)
        {
            DropWeapon(weaponThrown);
            weaponThrown = _weapon;
            Holster(WeaponSlot.Thrown, !(activeWeapon == WeaponSlot.Thrown));
            return;
        }

        // If this weapon is two-handed
        if (_weapon.weapon.isTwoHanded)
        {
            // Swap with the secondary weapon if it's active
            if (activeWeapon == WeaponSlot.Secondary)
            {
                DropWeapon(weaponSecondary);
                weaponSecondary = _weapon;
                Holster(WeaponSlot.Secondary, !(activeWeapon == WeaponSlot.Secondary));
            }
            // Otherwise swap with the primary weapon
            else
            {
                DropWeapon(weaponPrimary);
                weaponPrimary = _weapon;
                Holster(WeaponSlot.Primary, !(activeWeapon == WeaponSlot.Primary));
            }
        }
        // If this weapon is one-handed
        else
        {
            DropWeapon(weaponSmall);
            weaponSmall = _weapon;
            Holster(WeaponSlot.Small, !(activeWeapon == WeaponSlot.Small));
        }

        // Update UI - weapon, remaining ammo, current clip
        uiManager.UpdateWeapon(CurrentWeapon());
    }


    /// <summary>Drop the weapon.</summary>
    /// <param name="_weapon">The weapon to be dropped.</param>
    public void DropWeapon (Weapon _weapon)
    {
        if (_weapon == null)
        {
            return;
        }

        // Disconnect it from the player
        _weapon.transform.parent = null;

        // Activate the rigidbody
        _weapon.RigidbodyActive(true);

        // Throw it onto the floor
        // TODO - it will just drop for now, it should fly a little in front of the player

        // Update UI - weapon, current clip, remaining ammo
        uiManager.UpdateWeapon(CurrentWeapon());
    }


    /// <summary>Gets the current weapon.</summary>
    /// <returns>Returns a the current weapon.</returns>
    public Weapon CurrentWeapon ()
    {
        return GetWeapon(activeWeapon);
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}