﻿/* 
 * Script: Weapon
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public enum Type
    {
        Melee,
        Ranged,
        Thrown
    }

	// Public Variables
	public WeaponObject weapon;
	
    // Private Variables
    private bool canPickUp;
    private GameObject player;
    private Rigidbody rb;

    
    // Methods
    void Awake ()
    {
        // TODO - rb = GetComponent<Rigidbody>(); not working in Start, work out why

        canPickUp = false;
        rb = GetComponent<Rigidbody>();
    }


    void Update ()
    {
        if (canPickUp && Input.GetKeyDown(KeyCode.E))
        {
            PickUp();
        }
    }


    void OnTriggerEnter (Collider _collider)
    {
        canPickUp = true;
        player = _collider.transform.root.gameObject;
    }


    void OnTriggerExit (Collider _collider)
    {
        canPickUp = false;
        player = null;
    }


    private void PickUp ()
    {
        // Don't continue if the player is not valid
        if (player == null)
        {
            Debug.LogWarning("Can't pick up " + weapon.name + ", player is null");
            return;
        }

        // Disable the rigidbody
        RigidbodyActive(false);

        // Send it off to the weapon controller
        WeaponController playerWeapons = player.GetComponent<WeaponController>();
        playerWeapons.TakeWeapon(this);
    }


    /// <summary>Base attack class, handled by the sub-classes.</summary>
    /// <param name="_desiredRange">A desired attack range, 0 by default.</param>
    public virtual void Attack (float _desiredRange=0)
    {
        print("Sending to subclass...");
    }


    public virtual int[] GetAmmo ()
    {
        print("Sending to subclass...");

        return new int[2] {0, 0};
    }



    /// <summary>Set the colour for the particles and activate them</summary>
    public void DisplayRarityParticles ()
    {
        // TODO - work out how this'll work... not too important right now...
    }


    public void RigidbodyActive (bool _state)
    {
        rb.isKinematic = !_state;       // Is it controlled by code
        rb.detectCollisions = _state;   // Does it detect collisions
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}