﻿/* 
 * Script: UIButtonTextEffects
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class UIButtonTextEffects : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    // Public Variables
    public TextMeshProUGUI targetText;

    [Header("Colour Fade On Hover")]
	public bool changeColour = false;
    public Color hoverColour = Color.white;
    public float changeColourSpeed = 1.0f;

    [Header("Letter Spacing On Hover")]
    public bool changeLetterSpacing = false;
    public float hoverSpacingAmount = 1.0f;
    public float changeLetterSpacingSpeed = 1.0f;


	// Private Variables
    private Color originalColor;
    private float originalLetterSpacing;
    private float targetLetterSpacing;


    // Methods
    void Start ()
    {
        // Bail if the target text isn't set
        if (!targetText)
        {
            return;
        }

        originalColor = targetText.color;
        originalLetterSpacing = targetText.characterSpacing;
        targetLetterSpacing = originalLetterSpacing;
    }


    void Update ()
    {
        if (changeLetterSpacing)
        {
            float current = targetText.characterSpacing;
            targetText.characterSpacing = Mathf.Lerp(current, targetLetterSpacing, changeLetterSpacingSpeed);
        }
    }

    
    public void OnPointerEnter (PointerEventData _event)
    {
        if (changeColour)
        {
            targetText.CrossFadeColor(hoverColour, changeColourSpeed, false, false);
        }

        if (changeLetterSpacing)
        {
            targetLetterSpacing = hoverSpacingAmount;
        }
    }


    public void OnPointerExit (PointerEventData _event)
    {
        if (changeColour)
        {
            targetText.CrossFadeColor(originalColor, changeColourSpeed, false, false);
        }

        if (changeLetterSpacing)
        {
            targetLetterSpacing = originalLetterSpacing;
        }
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}