﻿/* 
 * Script: UIMainMenu
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMainMenu : MonoBehaviour
{
	// Public Variables
	

	// Private Variables
    

    // Methods
    public void LoadCanvas (string _canvasName)
    {
        switch (_canvasName)
        {
            case "singleplayer":
                print("Load singleplayer menu...");
                break;
            
            case "multiplayer":
                print("Load multiplayer menu...");
                break;
            
            case "collection":
                print("Load collection menu...");
                break;
            
            case "options":
                print("Load options menu...");
                break;
            
            case "quit":
                print("Quit game...");
                Application.Quit();
                break;
            
            default:
                Debug.LogWarning("\"" + _canvasName + "\" does not match a main menu canvas.");
                break;
        }
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}