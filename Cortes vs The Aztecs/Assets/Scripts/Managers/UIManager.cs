﻿/* 
 * Script: UIManager
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
	// Public Variables
    [Header("Text Fields")]
    public TextMeshProUGUI valueScore;
    public TextMeshProUGUI valueGold;
    public TextMeshProUGUI valueWeaponName;
    public TextMeshProUGUI valueClip;
    public TextMeshProUGUI valueAmmo;
    public TextMeshProUGUI valueBlood;

    [Header("Effect Settings")]
    public float fadeTextDuration = 0.5f;

	// Private Variables
    private static UIManager instance;
    


    // Methods
    private void Awake ()
    {
        // Prepare singleton
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
    }


    private void DisplayWeapon (bool _clearName=true, bool _clearClip=true, bool _clearAmmo=true)
    {
        // Weapon name
        if (_clearName)
        {
            valueWeaponName.CrossFadeAlpha(1, fadeTextDuration, false);
        }
        else
        {
            valueWeaponName.CrossFadeAlpha(0, fadeTextDuration, false);
        }

        // Clip remaining
        if (_clearClip)
        {
            valueClip.CrossFadeAlpha(1, fadeTextDuration, false);
        }
        else
        {
            valueClip.CrossFadeAlpha(0, fadeTextDuration, false);
        }

        // Ammo remaining
        if (_clearAmmo)
        {
            valueAmmo.CrossFadeAlpha(1, fadeTextDuration, false);
        }
        else
        {
            valueAmmo.CrossFadeAlpha(0, fadeTextDuration, false);
        }
    }


    public static UIManager Instance
    {
        get { return instance; }
    }
    

    public void UpdateScore (int _score)
    {
        valueScore.SetText(_score.ToString());
    }


    public void UpdateWaveCount (int _waveNum)
    {

    }


    public void UpdateGold (int _goldAmount)
    {
        valueGold.SetText(_goldAmount.ToString());
    }


    public void UpdateAmmo (int _clipRemaining=0, int _ammoRemaining=0, bool _isDisplayed=true)
    {
        // Display/hide the UI elements
        if (_isDisplayed)
        {
            valueClip.alpha = 1;
            valueAmmo.alpha = 1;
        }
        else
        {
            valueClip.alpha = 0;
            valueAmmo.alpha = 0;
        }

        // Set the new values
        valueClip.SetText(_clipRemaining.ToString());
        valueAmmo.SetText(_ammoRemaining.ToString());
    }


    public void UpdateBloodLevel (int _bloodLevel, int _maxBloodLevel)
    {
        int percentage = Mathf.RoundToInt(((float)_bloodLevel / _maxBloodLevel) * 100.0f);
        valueBlood.SetText(percentage + "%");
    }


    public void DisplayPowerup ()
    {

    }


    public void UpdateWeapon (Weapon _weapon)
    {
        if (_weapon == null)
        {
            DisplayWeapon(false, false, false);
        }
        else
        {
            // Weapon name
            valueWeaponName.SetText(_weapon.weapon.name);
            
            // Ammo counts
            switch (_weapon.weapon.type)
            {
                case Weapon.Type.Ranged:
                    // Gets current clip [0] and remaining ammo [1]
                    int[] ammo = _weapon.GetAmmo();
                    UpdateAmmo(ammo[0], ammo[1]);
                    DisplayWeapon(true, true, true);
                    break;
                
                case Weapon.Type.Melee:
                    DisplayWeapon(_clearClip:false, _clearAmmo:false);
                    break;

                case Weapon.Type.Thrown:
                    DisplayWeapon(_clearAmmo:false);
                    break;
            }
        }
    }


    public void Reset ()
    {
        UpdateScore(0);
        UpdateWaveCount(0);
        UpdateGold(0);
        UpdateBloodLevel(0, 0);
        UpdateWeapon(null);
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}