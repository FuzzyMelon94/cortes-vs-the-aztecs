﻿/* 
 * Script: WeaponManager
 * Author: Tom Brown
 * Description:
 * TODO:
 *      - May need to break the lists of weapons into rarities too
 *      - 
*/

using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public LayerMask weaponHitLayers;   // Layers that can be hit by weapons

	// Public Variables
    [Header("Weapons")]
    public List<Weapon> pistols;
    public List<Weapon> shotguns;
    public List<Weapon> assaultRifles;
    public List<Weapon> snipers;
    public List<Weapon> throwables;


	// Private Variables
    private static WeaponManager instance;


    // Methods
    private void Awake ()
    {
        Singleton();
    }


    private void Singleton ()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
    }


    public static WeaponManager Instance
    {
        get { return instance; }
    }


    public Weapon GetRandomPistol ()
    {
        return pistols[Random.Range(0, pistols.Count)];
    }


    public Weapon GetRandomShotgun ()
    {
        return pistols[Random.Range(0, shotguns.Count)];
    }


    public Weapon GetRandomAssaultRifle ()
    {
        return pistols[Random.Range(0, assaultRifles.Count)];
    }


    public Weapon GetRandomSniper ()
    {
        return pistols[Random.Range(0, snipers.Count)];
    }


    public Weapon GetRandomThrowable ()
    {
        return pistols[Random.Range(0, throwables.Count)];
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}