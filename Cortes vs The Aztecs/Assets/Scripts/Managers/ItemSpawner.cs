﻿/* 
 * Script: ItemSpawner
 * Author: Tom Brown
 * Description: 
*/

using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
	// Public Variables
	public List<ItemRarityObject> itemRarities;

    public int common = 0;
    public int uncommon = 0;
    public int rare = 0;
    public int epic = 0;
    public int legendary = 0;
    public int unique = 0;
    public int total = 0;

	// Private Variables

    private static ItemSpawner instance;    // Singleton static


    // Methods
    private void Awake ()
    {
        // Make sure only one instance exists
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
    }


    private void Start ()
    {
        // Sort the rarity list - compare x to y, where x is the 1st item, y is the 2nd
        itemRarities.Sort((x, y) => x.dropRate.CompareTo(y.dropRate));

        for (int i=0; i<100; i++)
        {
            Vector2 randPos = Random.insideUnitCircle * 5;
            GenerateLoot(new Vector3(randPos.x, 0, randPos.y));
        }
    }


    private ItemRarityObject GetRarity (int _percentage)
    {
        ItemRarityObject rarity = itemRarities[itemRarities.Count-1];

        for (int i=0; i<itemRarities.Count; i++)
        {
            if (_percentage <= itemRarities[i].dropRate)
            {
                rarity = itemRarities[i];
                break;
            }
        }

        switch (rarity.name)
        {
            case "Common":
                common++;
                break;
            case "Uncommon":
                uncommon++;
                break;
            case "Rare":
                rare++;
                break;
            case "Epic":
                epic++;
                break;
            case "Legendary":
                legendary++;
                break;
            case "Unique":
                unique++;
                break;
        }

        total ++;

        return rarity;
    }


    public static ItemSpawner Instance
    {
        get { return instance; }
    }


    public void GenerateLoot (Vector3 _spawnPos)
    {
        // Calculate coin drop

        // Calculate health drop

        // Calculate ammo drop

        // Calculate item drop ----------------
        if (itemRarities == null && itemRarities.Count <= 0)
        {
            return;
        }

        // Random from 1 to 100
        int dropNum = Random.Range(1, 101);

        // Get the rarity level
        ItemRarityObject rarity = GetRarity(dropNum);

        // Spawn the associated prefab at the location
        GameObject newDrop = Instantiate(rarity.dropPrefab, _spawnPos, Quaternion.identity);
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}