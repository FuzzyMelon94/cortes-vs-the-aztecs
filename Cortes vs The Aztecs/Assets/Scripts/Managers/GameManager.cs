﻿/* 
 * Script: GameManager
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	// Public Variables
    [Header("Game Settings")]
    public int bloodMax = 1000;     // How much blood to game over
    [Range(1.0f, 2.0f)]
    public float difficultyMultiplier = 1;   // A difficulty booster

    [Header("Time Settings")]
    public float waitBeforeGameStart = 3;    // Seconds before starting
    public float waitBetweenWaves = 1;      // Seconds before next wave
    public int traderWaveEvery = 5; // Every x round trader appears

    [Header("Spawn Settings")]
    public int traderWave = 5;      // Spawn a Trader on multiple of x waves
    public int bossWave = 5;        // Spawn Bosses on multiple of x waves
    public int miniBossOffset = 1;  // Spawn Mini Bosses x waves before boss
    public int baseStandard = 15;   // Base number of standard enemies
    public int addStandard = 5;     // How many standard enemies to add per wave
    public int baseMiniBoss = 2;    // Base number of standard enemies
    public int addMiniBoss = 2;     // How many standard enemies to add per wave
    public int baseBoss = 1;        // Base number of standard enemies
    public int addBoss = 1;         // How many standard enemies to add per wave
    public EnemySpawner[] spawners; // An array of possible spawners

    [Header("Altars")]
    public Altar altar1;       // The first altar to rise
    public Altar altar2;       // The second altar to rise
    public Altar altar3;       // The third altar to rise
    public Altar altar4;       // The fourth altar to rise


	// Private Variables
    private static GameManager instance;    // Singleton static
    private UIManager uiManager;

    private EnemySpawner currentSpawner;    // Spawner in use
    [Header("Serialised Fields")]
    [SerializeField]
    private int currentScore;   // The players score
    [SerializeField]
    private int waveCount;      // The wave the player has reached
    [SerializeField]
    private int goldCount;      // The players gold reserves
    [SerializeField]
    private int bloodLevel;     // The level of the blood
    private PlayerController player;  // The player
    private int altarsRisen;


    // Methods
    private void Awake ()
    {
        // Make sure only one instance exists
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
    }


    private void Start ()
    {
        uiManager = UIManager.Instance;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        // Log a warning if the manager doesn't have any spawners
        if (spawners.Length < 0)
        {
            Debug.LogWarning("No spawners linked to GameManager, prepare for the unexpected.");
        }        

        // Set the starter variables
        Reset();
        
        if (difficultyMultiplier > 1 && difficultyMultiplier < 2)
        {
            difficultyMultiplier = 1.5f;
        }

        // Start the game
        //NextWave(waitBeforeGameStart);
    }


    public static GameManager Instance
    {
        get { return instance; }
    }


    public void Reset ()
    {
        // Reset variables
        currentScore = 0;
        waveCount = 0;
        goldCount = 0;
        bloodLevel = 0;
        altarsRisen = 0;

        // Reset UI
        uiManager.Reset();
    }


    public void IncreaseScore (int _amount)
    {
        currentScore += _amount;
        uiManager.UpdateScore(currentScore);
    }


    public void IncreaseGold (int _amount)
    {
        goldCount += _amount;
        uiManager.UpdateGold(goldCount);
    }


    public void IncreaseBloodLevel (int _amount)
    {
        bloodLevel += _amount;

        uiManager.UpdateBloodLevel(bloodLevel, bloodMax);

        // Raise altars at 25%, 50%, 75%, 100%
        if (altarsRisen <= 0 && (float)bloodLevel/(float)bloodMax > 0.25f)
        {
            altar1.Raise();
            altarsRisen = 1;
        }

        if (altarsRisen == 1 && (float)bloodLevel/(float)bloodMax > 0.5f)
        {
            altar2.Raise();
            altarsRisen = 2;
        }

        if (altarsRisen == 2 && (float)bloodLevel/(float)bloodMax > 0.75f)
        {
            altar3.Raise();
            altarsRisen = 3;
        }

        if ((float)bloodLevel/(float)bloodMax >= 1f)
        {
            altar4.Raise();
            altarsRisen = 4;
        }
    }


    // ===== BEGIN WAVE MANAGEMENT ===== //
    public void NextWave (float _timeBetweenWaves)
    {
        int countStandard = 0;
        int countMiniBoss = 0;
        int countBoss = 0;
        waveCount ++;
        currentSpawner = spawners[Random.Range(0, spawners.Length)];
        
        // Give the player a heads up in the UI
        print("Next wave will be coming from the " + currentSpawner.spawnDirection + ".");

        // Calculate enemy numbers for this wave
        countStandard = baseStandard + (waveCount * Mathf.RoundToInt(addStandard * difficultyMultiplier));
        
        if (waveCount % bossWave == bossWave - miniBossOffset)
        {
            int miniBossWaves = ((waveCount + miniBossOffset) / bossWave) - 1;
            countMiniBoss = baseMiniBoss + (miniBossWaves * Mathf.RoundToInt(addMiniBoss * difficultyMultiplier));
        }

        if (waveCount % bossWave == 0)
        {
            int bossWaves = (waveCount / bossWave) - 1;
            countBoss = baseBoss + (bossWaves * Mathf.RoundToInt(addBoss * difficultyMultiplier));
        }

        print("S: " + countStandard + "  M: " + countMiniBoss + "  B: " + countBoss);

        // Prepare the spawner
        currentSpawner.SpawnEnemies(countStandard, countMiniBoss,
                                    countBoss, _timeBetweenWaves);

        // Update the UI
        uiManager.UpdateWaveCount(waveCount);
    }


    public void WaveComplete ()
    {
        print("Wave complete!");
        // Check if trader should be displayed
        if (waveCount > traderWave && waveCount % traderWave == 1)
        {
            // Do stuff to show trader
            print("Display Trader Joe");

            // Wait for user interaction to start -- This is temp
            NextWave(waitBetweenWaves * 2);
            print("Start the next wave -- " + waveCount);
        }
        else
        {
            NextWave(waitBetweenWaves);
            print("Start the next wave -- " + waveCount);
        }
    }
    // ===== END WAVE MANAGEMENT ===== //


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}