﻿/* 
 * Script: AmmoManager
 * Author: Tom Brown
 * Description: 
*/

using System.Collections.Generic;
using UnityEngine;

public class AmmoManager : MonoBehaviour
{
	// Public Variables
	[Header("Max Ammo")]
	public int maxAmmoPistol = 120;
    public int maxAmmoShotgun = 60;
    public int maxAmmoAssault = 240;
    public int maxAmmoSniper = 60;
    public int maxAmmoThrowable = 5;

    [Header("Default Ammo Types")]
    public AmmoObject defaultPistol;
    public AmmoObject defaultShotgun;
    public AmmoObject defaultAssault;
    public AmmoObject defaultSniper;
    public AmmoObject defaultThrowable;


	// Private Variables
    private static AmmoManager instance;


    // Methods
    private void Awake ()
    {
        Singleton();
    }


    private void Start ()
    {

    }


    private void Singleton ()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
    }


    public static AmmoManager Instance
    {
        get { return instance; }
    }
}