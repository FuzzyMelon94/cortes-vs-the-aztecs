﻿/* 
 * Script: Ammo
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class Ammo : MonoBehaviour
{
	public enum Type
    {
        Pistol,
        Shotgun,
        Assault,
        Sniper,
        Throwable
    }
    
    // Public Variables
	
    

	// Private Variables
    

    // Methods
    




    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}