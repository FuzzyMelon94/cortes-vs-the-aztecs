﻿/* 
 * Script: EnemyObjective
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class EnemyObjective : MonoBehaviour
{
    public enum ObjectiveType
    {
        Attackable,
        Destructible,
        Sacrificial
    }

	// Public Variables
	public ObjectiveType objectiveType = ObjectiveType.Sacrificial;

	// Private Variables
    

    // Methods
    void OnTriggerEnter (Collider _collider) 
    {
        if (_collider.transform.root.tag == "Enemy")
        {
            EnemyController enemy = _collider.transform.root.gameObject.GetComponent<EnemyController>();
            switch (objectiveType)
            {
                case ObjectiveType.Attackable:
                enemy.AttackObjective(gameObject);
                break;

                case ObjectiveType.Destructible:
                enemy.DestroyObjective(gameObject);
                break;

                case ObjectiveType.Sacrificial:
                enemy.SacrificeObjective();
                break;
            }
        }
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}