﻿/* 
 * Script: EnemySpawner
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public enum SpawnDirection
    {
        North,
        East,
        South,
        West
    }

	// Public Variables
    public float spawnWait;         // Time until next spawn
    public SpawnDirection spawnDirection;   // Which point of the compas the spawn is
    public float maxStairOffset = 10; // Maximum stair width/2
    public GameObject stairTop;     // Where to go

    [Header("Enemy Prefabs")]
    public GameObject enemyStandard;    // Deault enemy prefab
    public GameObject enemyMiniBoss;    // Miniboss prefab
    public GameObject enemyBoss;        // Big boss prefab
    

	// Private Variables
    private GameManager gameManager;
    private int activeSpawners;         // Number of active spawners
    private int spawnedStandard;        // Standard enemy spawn count
    private int spawnedMiniBoss;        // Mini Boss enemy spawn count
    private int spawnedBoss;            // Boss enemy spawn count
    private Quaternion spawnRotation;   // Look direction from spawn
    private Vector3 stairOffset;        // A random offset to fill the stairs
    

    // Methods
    private void Start ()
    {
        gameManager = GameManager.Instance;

        switch (spawnDirection)
        {
            // Point from North (Forward) towards centre (0,0,0)
            case SpawnDirection.North:
            spawnRotation = Quaternion.Euler(Vector3.back);
            break;

            // Point from East (Right) towards centre (0,0,0)
            case SpawnDirection.East:
            spawnRotation = Quaternion.Euler(Vector3.left);
            break;

            // Point from South (Back) towards centre (0,0,0)
            case SpawnDirection.South:
            spawnRotation = Quaternion.Euler(Vector3.forward);
            break;

            // Point from West (Left) towards centre (0,0,0)
            case SpawnDirection.West:
            spawnRotation = Quaternion.Euler(Vector3.right);
            break;
        }

        // ====== DEBUG ======
        isRunning = true;
    }


    private IEnumerator Spawn (GameObject _prefab, int _count, float _startDelay)
    {
        bool spawnEnemies = true;
        int spawned = 0;

        // Register as active spawner
        activeSpawners ++;

        // Wait before starting the spawning
        yield return new WaitForSeconds(_startDelay);

        // Release the hounds!
        while (spawnEnemies)
        {
            // Spawn enemy
            NewEnemy(_prefab);
            spawned ++;

            // Pause between spawning
            yield return new WaitForSeconds(spawnWait);

            // Check if enough have spawned
            if (spawned > _count)
            {
                spawnEnemies = false;
            }
        }

        // Unregister as active spawner 
        activeSpawners --;

        if (activeSpawners <= 0)
        {
            // Make sure active spawners never drops below 0 if there's a whoops
            activeSpawners = 0;

            // Tell the GM the wave is done
            gameManager.WaveComplete();
        }
    }


    public void NewEnemy (GameObject _prefab)
    {
        stairOffset = Vector3.zero;

        // Adjust spawn offset for direction
        if (spawnDirection == SpawnDirection.North || spawnDirection == SpawnDirection.South)
        {
            stairOffset.x = Random.Range(-maxStairOffset, maxStairOffset);
        }
        else if (spawnDirection == SpawnDirection.East || spawnDirection == SpawnDirection.West)
        {
            stairOffset.z = Random.Range(-maxStairOffset, maxStairOffset);
        }

        // Create a new enemy <= SHOULD BE AN OBJECT POOL
        GameObject newSpawn = Instantiate(_prefab,
                                          transform.position + stairOffset,
                                          spawnRotation);
        newSpawn.GetComponent<EnemyController>().ClimbingTarget(stairTop, stairOffset);
    }


    public void SpawnEnemies (int _numStandard, int _numMiniBoss,
                              int _numBoss, float _startDelay)
    {
        // Start the spawners
        if (_numStandard > 0)
        {
            StartCoroutine(Spawn(enemyStandard, _numStandard, _startDelay));
        }

        if (_numMiniBoss > 0)
        {
            StartCoroutine(Spawn(enemyMiniBoss, _numMiniBoss, _startDelay));
        }

        if (_numBoss > 0)
        {
            StartCoroutine(Spawn(enemyBoss, _numBoss, _startDelay));
        }
    }


    #region Debug Variables
    [Header("Debug Tools")]
    public bool showPath = true;
    private bool isRunning = false;
    #endregion

    #region Debug Methods
    void OnDrawGizmos ()
    {
        if (isRunning)
        {
        }

        if (stairTop && showPath)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, 1f);
            Gizmos.DrawLine(transform.position, stairTop.transform.position);
            Gizmos.DrawSphere(stairTop.transform.position, 1f);
        }
    }
    #endregion
}