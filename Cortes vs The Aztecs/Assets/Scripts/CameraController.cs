﻿/* 
 * Script: CameraController
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Public Variables
    public GameObject target;
    [Header("Smoothing")]
    public float movementSmoothing;
    public float rotationSmoothing;
    [Header("Position")]
    public float heightOffset = 15f;
    public float positionOffset = -15f;

    // Private Variables
    private Camera mainCam;

    // Methods
    void Start()
    {
        mainCam = Camera.main;
    }


    void Update()
    {
        UpdatePosition();
        UpdateRotation();
    }


    private void UpdatePosition ()
    {
        Vector3 newPosition = new Vector3(target.transform.position.x,
                                          target.transform.position.y + heightOffset,
                                          target.transform.position.z + positionOffset);

        mainCam.transform.position = newPosition;
    }


    private void UpdateRotation ()
    {
        mainCam.transform.LookAt(target.transform.position);
    }
    

    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}