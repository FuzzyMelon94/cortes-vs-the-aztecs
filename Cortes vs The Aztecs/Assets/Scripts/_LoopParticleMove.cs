﻿/* 
 * Script: _LoopParticleMove
 * Author: Tom Brown
 * Description: 
*/

using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class _LoopParticleMove : MonoBehaviour
{
    public enum LoopMode
    {
        PingPong,
        Loop
    }


	// Public Variables
    public bool ascending;
    public float moveSpeed;
    public LoopMode loopMode;
    public Transform[] pathPoints;    
    public GameObject[] projectiles;

    [Header("DEBUG OPTIONS")]
    public bool showPath = false;


	// Private Variables
    private bool isIncreasing;
    private int nextPos;
    private List<ParticleSystem> particles;
    

    // Methods
    void Start ()
    {
        if (pathPoints.Length < 2)
        {
            Debug.LogError("At least 2 points are required for a valid path.");
        }

        isIncreasing = ascending;
        nextPos = 1;
        particles = new List<ParticleSystem>();

        foreach (GameObject projectile in projectiles)
        {
            // Reset each projectiles position
            projectile.transform.position = pathPoints[0].position;
            
            // Rotate each projectile to look at the destination
            Vector3 relativePos = pathPoints[nextPos].position - projectile.transform.position;
            projectile.transform.rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        }        
    }


    void Update ()
    {
        // Move all the projectiles in the array
        foreach (GameObject projectile in projectiles)
        {
            // Move each projectile towards the end position
            projectile.transform.position = Vector3.MoveTowards(projectile.transform.position,
                                                                pathPoints[nextPos].position,
                                                                moveSpeed * Time.deltaTime);

            // When the projectiles reach their destination, update to the next point
            if (projectile.transform.position == pathPoints[nextPos].position)
            {
                switch (loopMode)
                {
                    // Cyclically move through the path points, wrapping between first and last
                    case LoopMode.Loop:
                        if (ascending)
                        {
                            nextPos++;
                        }
                        else
                        {
                            nextPos--;
                        }
                        break;

                    // Move from first to last, then last to first, and repeat
                    case LoopMode.PingPong:
                        if (nextPos == 0)
                        {
                            isIncreasing = !isIncreasing;
                        }

                        if (isIncreasing)
                        {
                            nextPos++;
                        }
                        else 
                        {
                            nextPos--;
                        }
                    break;
                }

                if (nextPos >= pathPoints.Length)
                {
                    nextPos = 0;
                }
                else if (nextPos < 0)
                {
                    nextPos = pathPoints.Length-1;
                }
            }
        }
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    void OnDrawGizmos ()
    {
        Color pointColour = Color.yellow;
        Color lineColour = Color.white;
        float pointSize = 0.25f;

        // If there's more than 2 points, draw the path
        if (showPath && pathPoints.Length > 2)
        {
            // Draw the first point (no line)
            Gizmos.color = pointColour;
            Gizmos.DrawSphere(pathPoints[0].position, pointSize);

            // Draw intermediary points and lines
            for (int i=1; i<pathPoints.Length; i++)
            {
                // Draw a point
                Gizmos.color = pointColour;
                Gizmos.DrawSphere(pathPoints[i].position, pointSize);

                // Draw a line from the previous point to the next
                Gizmos.color = lineColour;
                Gizmos.DrawLine(pathPoints[i-1].position, pathPoints[i].position);
            }

            // Draw a line between the first and last point
            Gizmos.color = lineColour;
            Gizmos.DrawLine(pathPoints[0].position, pathPoints[pathPoints.Length-1].position);
        }
    }
    #endregion
}