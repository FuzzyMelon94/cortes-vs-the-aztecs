﻿/* 
 * Script: Altar
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

public class Altar : MonoBehaviour
{
	// Public Variables
    public float riseSpeed = 5.0f;
    public float waitForFlame = 3.0f;
    public Transform risenTransform;
    public ParticleSystem flames;
	

	// Private Variables
    private bool isRising;
    

    // Methods
    private void Start ()
    {
        isRising = false;
    }


    private void Update ()
    {
        if (isRising)
        {
            transform.position = Vector3.Lerp(transform.position, risenTransform.position,
                                              riseSpeed * Time.deltaTime);

            if (Vector3.Distance(transform.position, risenTransform.position) < 0.05f)
            {
                isRising = false;
                flames.Play();
            }
        }
    }


    public void Raise ()
    {
        isRising = true;
    }


    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}