﻿/* 
 * Script: MeleeWeaponObject
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Melee Weapon", menuName = "Weapons/Melee", order=1)]
public class MeleeWeaponObject : WeaponObject
{
    [Header("Temporary/Experimental")]
	// Public Variables
    [Range(0, 5)]
    public int comboCount = 3;      // Number of hits per combo
}