﻿/* 
 * Script: ThrownWeaponObject
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Thrown Weapon", menuName = "Weapons/Thrown", order=2)]
public class ThrownWeaponObject : WeaponObject
{
	// Public Variables
	

	// Private Variables
    

    // Methods
    




    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}