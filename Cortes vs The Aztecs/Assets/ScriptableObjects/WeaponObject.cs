/* 
 * Script: WeaponObject
 * Author: Tom Brown
 * Description: 
*/

using UnityEngine;

public class WeaponObject : ScriptableObject
{   
    // Public Variables
    [Header("UI Variables")]
    public new string name = "New Weapon";              // Title for this weapon
    public string description = "Ooo! A new weapon.";   // Description of this weapon
    public ItemRarityObject rarity;                     // Spawn rate for this weapon
    public Sprite thumbnail;                            // Thumbnail to display

    [Header("Stats [Generic]")]
    public Weapon.Type type = Weapon.Type.Melee;  // Weapon type
    public bool isTwoHanded = false;            // If two hands are required for use
    [Range(1, 10)]
    public int damage = 5;                      // Base damage on impact
    [Range(1, 50)]
    public int range;                         // Optimal range in metres
    [Range(0, 5)]
    public int knockback;                       // Enemy knockback in metres
    [Range(0.01f, 5.0f)]
    public float fireRate = 2.0f;               // Seconds to wait before next 'fire'
}