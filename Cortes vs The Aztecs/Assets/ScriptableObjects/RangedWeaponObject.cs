﻿/* 
 * Script: RangedWeaponObject
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ranged Weapon", menuName = "Weapons/Ranged", order=2)]
public class RangedWeaponObject : WeaponObject
{
    // Public Variables
    [Header("Stats [Ranged]")]
    public Ammo.Type ammoType = Ammo.Type.Pistol;
    public AmmoObject defaultAmmo;  // Default ammo used by this gun
    [Range(1, 100)]
    public int accuracy = 100;      // Accuracy percentage - can be modified with ammo/mods
    [Range(1, 30)]
    public int clipSize = 6;        // Number of shots before reloading
    [Range(0.5f, 5.0f)]
    public float reloadTime = 2.0f; // Time to reload in seconds
    [Range(0, 60)]
    public int muzzleSpread = 0;    // Max angle of a projectile leaving the muzzle
    [Range(50, 100)]
    

    // --- To be refactored/implemented later --- //
    //  damageFalloff should reduce damage based on the curve after the 'optimal range'

    [Header("Temporary/Experimental")]
    public AnimationCurve damageFalloff;    // How quickly damage reduces out of range
}