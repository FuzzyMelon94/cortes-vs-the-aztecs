﻿/* 
 * Script: ItemRarityObject
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item Rarity", menuName = "Item Rarity")]
public class ItemRarityObject : ScriptableObject
{
    public new string name = "Common";  // Name of this rarity level
    public Color colour = Color.white;  // Colour used by this rarity level
    public int dropRate = 100;          // Drops per 100 - basic drop chance
    public GameObject dropPrefab;       // Prefab to use when dropped
}