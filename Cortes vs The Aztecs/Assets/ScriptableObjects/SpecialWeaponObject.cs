﻿/* 
 * Script: SpecialWeaponObject
 * Author: Tom Brown
 * Description: 
*/

using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Special Weapon", menuName = "Weapons/Special", order=3)]
public class SpecialWeaponObject : ScriptableObject
{
	// Public Variables
	

	// Private Variables
    

    // Methods
    




    #region Debug Variables
    #endregion

    #region Debug Methods
    #endregion
}