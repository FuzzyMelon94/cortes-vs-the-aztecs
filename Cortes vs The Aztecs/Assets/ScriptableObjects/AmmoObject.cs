﻿/* 
 * Script: AmmoObject
 * Author: Tom Brown
 * Description:
*/

using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ammo", menuName = "Weapons/Ammo", order=0)]
public class AmmoObject : ScriptableObject
{
    [Header("UI Settings")]
    public new string name = "New Ammo";
    public string description = "Standard ammo.";
    public Sprite thumbnail;

    [Header("Shell Settings")]
    public GameObject shellCasingPrefab;
    public float timeToLive;    // TODO - Decide if this should be a global 'shell TTL' variable

    [Header("Ammo Settings")]
    public GameObject projectilePrefab;
    public StatusEffectObject statusEffect;
    [Range(1, 15)]    
    public int projectileCount = 1;
    [Range(0.0f, 10.0f)]
    public float aoeRange = 0.0f;

    [Header("Stat Modifiers")]
    [Range(-10, 10)]
    public int accuracy = 0;
    [Range(-5.0f, 5.0f)]
    public float damage = 1.0f;
    [Range(-5.0f, 5.0f)]
    public float knockback = 1.0f;
    [Range(-5.0f, 5.0f)]
    public float range = 1.0f;
    [Range(1, 1000)]
    public int velocity = 500;
}