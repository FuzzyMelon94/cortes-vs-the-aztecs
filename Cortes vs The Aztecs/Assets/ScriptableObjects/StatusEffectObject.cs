﻿/* 
 * Script: StatusEffectObject
 * Author: Tom Brown
 * Description:
 * TODO:
 *      - Find a nice way of doing this, enums probs aren't the best...
*/

using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Status Effect", menuName = "Status Effect")]
public class StatusEffectObject : ScriptableObject
{
    public enum Status {
        None,
        Bleed,
        Burn,
        Freeze,
        Poison
    }

    public float duration;
    public float tickInterval;
    public int tickDamage;
    public Status status;

    // Particle/shader effects
    // Enemy movement speed.. somehow
}