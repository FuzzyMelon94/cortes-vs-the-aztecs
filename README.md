# Cortes vs The Aztecs

This was my first game jam

I’ve wanted to do one for a while but wasn’t free during many of the previous events.
I finally have some time off and decided to give this one a go.

Based on the theme of “Sacrifices must be made”, I decided that the player would play as Hernan Cortes (a blue cube with a fancy hat)

You must fend off hordes of Aztecs (red, green, and purple cubes) seeking to sacrifice themselves to the Gods (who I didn’t have time to implement).

The idea being this was their last ditch effort to defeat Cortes.

I had a lot of fun making this game and I got further than I thought I would, I also realised I need to tame my feature creep…

Sadly I didn’t have time to make any music, or sound, or a main menu, or a tutorial, or a bunch of other stuff… But there’s some mechanics!

[Download the game on itch.io](https://fuzzymelon94.itch.io/cortes-vs-the-aztecs)

[Check out the Ludum Dare 43 post here](https://ldjam.com/events/ludum-dare/43/cortes-vs-the-aztecs)